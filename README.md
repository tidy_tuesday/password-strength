# Tidy Tuesday 2020 - Week #3
### Password Strength
See https://github.com/rfordatascience/tidytuesday for more information.

I didn't quite trust the 'strength' column provided so I calculated bits of entropy based on the NIST Special Publication 800-63-2 (see https://en.wikipedia.org/wiki/Password_strength). The methodology appears to have been dropped 2017, but I couldn't find anything better.


![](password_strength.png)
## Code Snippet
```r

library(tidyverse)

passwords <- readr::read_csv('https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2020/2020-01-14/passwords.csv')

pw <- passwords %>% 
  filter(!is.na(password)) %>%
  mutate(online_crack_sec = case_when(time_unit == "years" ~ value * 365 / (24 * 60 * 60),
                                      time_unit == "months" ~ value * 30 / (24 * 60 * 60),
                                      time_unit == "weeks" ~ value * 7 / (24 * 60 * 60),
                                      time_unit == "days" ~ value / (24 * 60 * 60),
                                      time_unit == "hours" ~ value / (60 * 60),
                                      time_unit == "minutes" ~ value / 60,
                                      TRUE ~ value))

# https://en.wikipedia.org/wiki/Password_strength
bits_of_entropy <- function(pword) {
  if (!is.na(pword)) {
    ln <- nchar(pword)
    entropy = 0
    i = 1
    
    while (i <= ln){
      entropy = entropy + 
        case_when(i == 1 ~ 4,
                  i > 1 & i <= 8 ~ 2,
                  i > 8 & i <= 20 ~ 1.5,
                  TRUE ~ 1)
      i = i + 1
    }
    
    if (str_detect(pword,"[A-Z]") * str_detect(pword,"[a-z]") * str_detect(pword,"[0-9]")){
      entropy = entropy + 6
    }
    return(entropy)
  }
}

pw$entropy = sapply(pw$password, bits_of_entropy)

pw %>%
  ggplot(aes(category, entropy, fill=category)) +
  geom_violin(alpha = 0.6, col = my_bkgd, show.legend = F) +
  coord_flip() +
  scale_y_continuous(breaks = seq(10,20,2)) +
  labs(title = "Password Strength By Category",
       subtitle = "Average entropy = 14 across all categories",
       x = "", y = "Bits of Entropy\n(Based on NIST Special Publication 800-63-2)",
       caption = "Soures: Information is Beautiful, Wikipedia  |  By @DaveBloom11") +
  theme(panel.grid = element_line(color="gray30"),
        panel.border = element_rect(color="grey30", fill = NA))



```


